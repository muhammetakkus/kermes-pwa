import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Navbar from '@/components/Navbar.vue'

describe('Navbar.vue', () => {
  it('navbar msg props almalı', () => {
    const msg = 'q'
    const wrapper = shallowMount(Navbar, {
      propsData: { msg }
    })
    /* msg değişkenindeki metni içeriyor olmalı */
    expect(wrapper.text()).to.include(msg)
  })
})
