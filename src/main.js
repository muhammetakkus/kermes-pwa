import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

/* */
import firebase from 'firebase/app'
import 'firebase/firestore'

/* */
firebase.initializeApp({
  projectId: 'kermes-app',
  databaseURL: 'https://kermes-app.firebaseio.com'
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

/* */
const db = firebase.firestore()
db.settings({timestampsInSnapshots: true})
export { db }
